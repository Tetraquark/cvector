# Description: #
The implementation of the vector data structure in C language. 
A simple attempt to create a tool to work with dynamic arrays in C language. Cvector it's similarity of the vector data structure in a procedural language. 

# Compilation: #
The project includes the file Makefile that creates a static library in **./lib/** directory. 

Use example in the end of README.

# Описание: #
Реализация структуры данных вектора на языке C.
Это попытка упростить работу с динамическими массивами в языке C. Для этого и появился подобный инструмент Cvector для реализации аналога класса вектора в процедурном языке.

# Компиляция: #
В состав проекта входит простой Makefile, для утилиты make, с помощью которой можно скомпилировать статическую библиотеку cvector. После компиляции библиотека будет лежать в директории **./lib/**.

# Use example: #

```
#!c
#include <stdio.h> 		//for printf()
#include "cvector.h"	
int main(int argc, char *argv[]){

    cvector vector;
    int i = 0;
    //init vector of integers
    if(cvector_init(&vector, sizeof(int)) != FN_OK){
    	printf("Error in vector initialize\n");
    	return 1;
    }

    //save 10 elements in vector
    for(i = 0; i < 10; i++)
        cvector_push(&vector, (void*)&i);

    //output vector
    for(i = 0; i < cvector_size(&vector); i++){
        int *elem = (int*) cvector_get(&vector, i);
        if(elem != NULL)
            printf("%d\n", *elem);
    }

    //set last elem
    int lastElem = 888;
    if(cvector_set(&vector, cvector_size(&vector) - 1, (void*)&lastElem) == FN_OK){
    	printf("Success in Set: %d\n", *( (int*) cvector_get(&vector, cvector_size(&vector) - 1) ) );
    }
    else{
        printf("Fail in Set\n");
        cvector_clear(&vector);
        return 0;
    }

    //delete first element
    if(cvector_delete(&vector, 0) == FN_OK){
        printf("Success in Delete\n");
    }
    else{
        printf("Fail in Delete\n");
        cvector_clear(&vector);
        return 1;
    }

    //clear vector
    cvector_clear(&vector);
    return 0;
}

```
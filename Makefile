make : lib cvector.o
	ar rc lib/libcvector.a cvector.o
cvector.o : src/cvector.c include/cvector.h
	gcc -c -std=c99 src/cvector.c -o cvector.o
lib : lib
	mkdir lib
clean : 
	rm -r cvector.o lib

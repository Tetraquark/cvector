#include "../include/cvector.h"

/*
 * Private function for resizing vector.
 */
static fn_result_t cvector_resize(cvector* const __v, VECTOR_SIZE_TYPE __newCap){
	void* loc_ptr = realloc(__v->data, __newCap * sizeof(void *));
	if(loc_ptr == NULL)
		return FN_FAIL;

	__v->data = loc_ptr;
	__v->capacity = __newCap;
	return FN_OK;
}

fn_result_t cvector_init(cvector* __v, size_t __dataSize){
	void** loc_ptr = (void **) malloc(CVECTOR_INIT_CAPACITY * sizeof(void*));

	if(loc_ptr == NULL)
		return FN_FAIL;

	__v->data = loc_ptr;
	__v->capacity = CVECTOR_INIT_CAPACITY;
	__v->size = 0;
	__v->element_size = __dataSize;
	return FN_OK;
}

VECTOR_SIZE_TYPE cvector_size(cvector* const __v){
	return __v == NULL ? 0 : __v->size;
}

fn_result_t cvector_push(cvector* const __v, void* const __data){
	if(__v == NULL || __data == NULL)
		return FN_FAIL;

	if(__v->size >= __v->capacity){
		if(cvector_resize(__v, __v->capacity + __v->capacity / 2) == FN_FAIL){
			return FN_FAIL;
		}
	}

	void* loc_ptr = (void*) malloc(__v->element_size);
	if(loc_ptr == NULL)
		return FN_FAIL;

	__v->data[__v->size] = loc_ptr;
	memcpy(__v->data[__v->size], __data, __v->element_size);

	__v->size++;

	return FN_OK;
}

fn_result_t cvector_set(cvector* const __v, VECTOR_SIZE_TYPE __index, void* const __data){
	if(__v == NULL || __data == NULL)
		return FN_FAIL;

	if(__index >= 0 && __index < __v->size){
		memcpy(__v->data[__index], __data, __v->element_size);
		return FN_OK;
	}
	return FN_FAIL;
}

fn_result_t cvector_delete(cvector* const __v, VECTOR_SIZE_TYPE __index){
	if(__v == NULL || __index < 0 || __index > __v->size - 1 || __v->size <= 0)
		return FN_FAIL;

	void* loc_ptr = cvector_get(__v, __index);
	if(loc_ptr == NULL)
		return FN_FAIL;
	free(loc_ptr);

	VECTOR_SIZE_TYPE i = 0;
	for(i = __index; i < __v->size - 1; i++)
		__v->data[i] = __v->data[i + 1];

	cvector_resize(__v, __v->size - 1);
	__v->size--;
	return FN_OK;
}

void* cvector_get(cvector* const __v, VECTOR_SIZE_TYPE __index){
	if(__v == NULL || __index < 0 || __index > __v->size - 1 || __v->size <= 0)
		return NULL;

	return __v->data[__index];
}

fn_result_t cvector_clear(cvector* const __v){
	if(__v == NULL)
		return FN_FAIL;
	VECTOR_SIZE_TYPE i = 0;
	for(i = 0; i < __v->size; i++)
		free(__v->data[i]);
	__v->size = 0;
	free(__v->data);
	return FN_OK;
}

/*
 * 	The implementation of the vector data structure in C language.
 * 	cvector structure is analogue class object which contains array
 * 	of void pointers, which play data array role, and other "class" fields.
 *
 *	For example of use see README.md file.
 *
 *	@author		Tetraquark	| tetraquark.ru
 */

#ifndef CVECTOR_H_
#define CVECTOR_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define VECTOR_SIZE_TYPE uint32_t

/*!
 * Enum for output functions results.
 */
typedef enum{
	FN_OK = 0,
	FN_FAIL
} fn_result_t;

// Default vector capacity
#define CVECTOR_INIT_CAPACITY	4

/*
 * Structure for creating vector "objects".
 */
typedef struct{
	void** data;			//data array
	VECTOR_SIZE_TYPE size;				//size of vector
	VECTOR_SIZE_TYPE capacity;			//actual size of vector
	size_t element_size;	//vector element size in bytes
} cvector;

/*
 * Initialization function for cvector structure.
 *
 * @param __v			pointer on cvector structure
 * @param __dataSize	size in bytes of data element
 * @return FN_OK - successful initialization of vector, FN_FAIL - unsuccessful
 */
fn_result_t cvector_init(cvector* __v, size_t __dataSize);

/*
 * Return size (elements number) of the vector.
 *
 * @param __v	pointer on cvector structure
 * @return vector size or 0, if __v is NULL pointer
 */
VECTOR_SIZE_TYPE cvector_size(cvector* const __v);

/*
 * Create copy of data by the pointer and
 * save new data in the end of vector.
 *
 * @param __v		pointer on cvector structure
 * @param __data	pointer on the saved data
 * @return FN_OK - if saving data was successful, FN_FAIL - unsuccessful
 */
fn_result_t cvector_push(cvector* const __v, void* const __data);

/*
 * Setting the new value of an existing element
 * in the vector by index. If the operation is
 * successful the function returns 1, else returns 0.
 *
 * @param __v		pointer on cvector structure
 * @param __index	element index
 * @param __data	pointer on the saved data
 * @return FN_OK - if setting data was successful, FN_FAIL - unsuccessful
 */
fn_result_t cvector_set(cvector* const __v, VECTOR_SIZE_TYPE __index, void* const __data);

/*
 * Remove element from vector by index. Remaining
 * elements after the deleted element will be shifted
 * by -1. If the operation is successful the function
 * returns 1, else returns 0.
 *
 * @param __v		pointer on cvector structure
 * @param __index	element index
 * @return FN_OK - if deleting data was successful, FN_FAIL - unsuccessful
 */
fn_result_t cvector_delete(cvector* const __v, VECTOR_SIZE_TYPE __index);

/*
 * Get the data pointer of element by its index.
 * If the operation is not successful the function
 * returns NULL pointer.
 *
 * @param __v		pointer on cvector structure
 * @param __index	element index
 * @return void pointer on data or NULL pointer
 */
void* cvector_get(cvector* const __v, VECTOR_SIZE_TYPE __index);

/*
 * Clears vector and frees memory.
 *
 * @param __v		pointer on cvector structure
 * @return FN_OK - if clearing vector was successful, FN_FAIL - unsuccessful
 */
fn_result_t cvector_clear(cvector* const __v);

#endif /* CVECTOR_H_ */
